import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),

        ),
        body: Column (

          children: [
            Image.asset('assets/1.jpg',width: 600, height: 400,fit: BoxFit.cover),
            Text('นาย ธรรมสถิตย์ แสงจันทร์',style:TextStyle(height: 5, fontSize: 30)),
            Text('6350110026',style:TextStyle(fontSize: 30)),
          ],

        ),
      ),
    );
  }
}

